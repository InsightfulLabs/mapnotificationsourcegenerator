package com.tm.map.sourcegenerator.manager;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceAsync;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceAsyncClient;
import com.amazonaws.services.elasticmapreduce.model.AddJobFlowStepsRequest;
import com.amazonaws.services.elasticmapreduce.model.AddJobFlowStepsResult;
import com.amazonaws.services.elasticmapreduce.model.DescribeStepRequest;
import com.amazonaws.services.elasticmapreduce.model.DescribeStepResult;
import com.amazonaws.services.elasticmapreduce.model.StepConfig;
import com.tm.map.sourcegenerator.common.Constants;
import org.apache.commons.configuration.ConfigurationException;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class EMRManager {

    private static EMRManager instance;
    private AmazonElasticMapReduceAsync service;

    private EMRManager() throws ConfigurationException {
        service = new AmazonElasticMapReduceAsyncClient(new BasicAWSCredentials(Constants.AWS_ACCESS_KEY, Constants.AWS_SECRET_KEY));
        Regions region = Regions.fromName("us-east-1");
        service.setRegion(Region.getRegion(region));
    }

    public static EMRManager getInstance() {
        if (instance == null) {
            synchronized (EMRManager.class) {
                if (instance == null) {
                    try {
                        instance = new EMRManager();
                    } catch (ConfigurationException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
        return instance;
    }

    /**
     * Adds  a single step to the givven cluster id using the given step config.
     *
     * @param clusterId
     * @param stepConfig
     * @return Step Id created using the configurations.
     */
    public String addSingleStep(String clusterId, StepConfig stepConfig) {
        AddJobFlowStepsResult result = service.addJobFlowSteps(new AddJobFlowStepsRequest(clusterId).withSteps(stepConfig));
        return result.getStepIds().get(0);
    }

    public DescribeStepResult describeStepAsync(String clusterId, String stepId) throws ExecutionException, InterruptedException {
        DescribeStepRequest request = new DescribeStepRequest().withStepId(stepId).withClusterId(clusterId);
        Future<DescribeStepResult> future = service.describeStepAsync(request);
        return future.get();
    }
}
