package com.tm.map.sourcegenerator.launcher;

import com.tm.map.sourcegenerator.processor.job.AllMAPNotificationSourceComputer;

/**
 * @author <a href="mailto:puneet@targetingmantra.com">Puneet Chaurasia</a>
 */
public class AllMAPNotificationSourceComputerLauncher extends AbstractJobLauncher {

    public AllMAPNotificationSourceComputerLauncher(final int marketplaceId, final String startTime) {
        super(marketplaceId, startTime);
    }

    @Override
    protected String getJar() {
        return "s3://tm-emr/jars/source-generator/map-source-generator-processor.jar";
    }

    @Override
    protected String getClazz() {
        return AllMAPNotificationSourceComputer.class.getCanonicalName();
    }

    @Override
    protected String[] getArgs() {
        return new String[] {startTime};
    }
}
