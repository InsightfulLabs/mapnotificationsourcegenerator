package com.tm.map.sourcegenerator.launcher;

import com.tm.map.sourcegenerator.processor.job.ViewFollowupComputer;

/**
 * @author <a href="mailto:puneet@targetingmantra.com">Puneet Chaurasia</a>
 */
public class ViewFollowupSourceComputerLauncher extends AbstractJobLauncher {

    public ViewFollowupSourceComputerLauncher(final int marketplaceId, final String startTime) {
        super(marketplaceId, startTime);
    }

    @Override
    protected String getJar() {
        return "s3://tm-emr/jars/source-generator/map-source-generator-processor.jar";
    }

    @Override
    protected String getClazz() {
        return ViewFollowupComputer.class.getCanonicalName();
    }

    @Override
    protected String[] getArgs() {
        return new String[] {startTime};
    }
}
