package com.tm.map.sourcegenerator.launcher;

import com.amazonaws.services.elasticmapreduce.model.ActionOnFailure;
import com.amazonaws.services.elasticmapreduce.model.DescribeStepResult;
import com.amazonaws.services.elasticmapreduce.model.HadoopJarStepConfig;
import com.amazonaws.services.elasticmapreduce.model.StepConfig;
import com.amazonaws.services.elasticmapreduce.model.StepState;
import com.tm.map.sourcegenerator.common.Constants;
import com.tm.map.sourcegenerator.manager.EMRManager;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public abstract class AbstractJobLauncher {

    private static final Logger LOGGER = Logger.getLogger(AbstractJobLauncher.class);
    private static final String today = new SimpleDateFormat("yyyyMMdd").format(new Date());

    private static final int MIN_THREAD_DELAY_SECS = 2;
    private static final int MAX_THREAD_DELAY_SECS = 10 * 60;

    private static final List<StepState> DONE_STATES = Arrays.asList(StepState.COMPLETED,
            StepState.FAILED,
            StepState.CANCELLED);

    private int findNextSleep(int threadDelay) {
        if (threadDelay >= MAX_THREAD_DELAY_SECS) {
            return MAX_THREAD_DELAY_SECS;
        }
        threadDelay = (int) (1.5 * threadDelay);
        return (threadDelay > MAX_THREAD_DELAY_SECS) ? MAX_THREAD_DELAY_SECS : threadDelay;
    }

    private final int marketplaceId;
    protected final String startTime;

    protected AbstractJobLauncher(int marketplaceId, String startTime) {
        this.marketplaceId = marketplaceId;
        this.startTime = startTime;
    }

    private int checkStatus(String stepName, String stepId) {
        int errCount = 0;
        StepState prevState = null;
        int threadDelay = MIN_THREAD_DELAY_SECS;
        Long start = null;
        try {
            DescribeStepResult result = EMRManager.getInstance().describeStepAsync(Constants.CLUSTER_ID, stepId);
            while (true) {
                if (start != null && (System.currentTimeMillis() - start) > Constants.FOUR_HOUR_TIME_IN_MILLIS) {
                    start += Constants.ONE_HOUR_TIME_IN_MILLIS;
                }
                try {
                    StepState state = StepState.fromValue(result.getStep().getStatus().getState());
                    if (DONE_STATES.contains(state)) {
                        if (state != StepState.COMPLETED) {
                            String message = "Failed to generate " + stepName + ".\nMarket Id: " + this.marketplaceId;
                            LOGGER.error(message);
                            return 1;
                        }
                        return 0;
                    } else {
                        if (!state.equals(prevState)) {
                            threadDelay = MIN_THREAD_DELAY_SECS;
                            prevState = state;
                            if (state == StepState.RUNNING) {
                                start = System.currentTimeMillis();
                            }
                        }
                        LOGGER.info("Current State: " + state);
                        Thread.sleep(threadDelay * 1000);
                        threadDelay = findNextSleep(threadDelay);
                        result = EMRManager.getInstance().describeStepAsync(Constants.CLUSTER_ID, stepId);
                    }
                } catch (Exception e) {
                    errCount++;
                    LOGGER.error("Error loading step info", e);
                    if (errCount > 10) {
                        return 1;
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return 1;
    }

    protected StepConfig createStepConfig(String stepName, List<String> arguments, String jar, String mainClass, String marketplaceId) {
        StepConfig stepConfig = new StepConfig();
        stepConfig.setName(stepName + " Step-" + marketplaceId + " " + today);
        stepConfig.setActionOnFailure(ActionOnFailure.CONTINUE);
        HadoopJarStepConfig jarSetup = new HadoopJarStepConfig();
        jarSetup.setArgs(arguments);
        jarSetup.setJar(jar);
        if (null != mainClass) {
            jarSetup.setMainClass(mainClass);
        }
        stepConfig.setHadoopJarStep(jarSetup);
        return stepConfig;
    }


    private StepConfig createSparkStepConfig(String stepName, String jarName, String mainClass, String... args) {
        List<String> sparkArguments = new ArrayList<>();
        sparkArguments.addAll(Arrays.asList(
                "/home/hadoop/spark/bin/spark-submit",
                "--deploy-mode",
                "cluster",
                "--master",
                "yarn-cluster",
                "--class",
                mainClass,
                jarName,
                String.valueOf(this.marketplaceId)
        ));
        sparkArguments.addAll(Arrays.asList(args));
        StepConfig sparkConfig = new StepConfig();
        sparkConfig.setName("Spark-step: " + stepName + " " + today);
        sparkConfig.setActionOnFailure(ActionOnFailure.CONTINUE);
        HadoopJarStepConfig jarSetup = new HadoopJarStepConfig();
        jarSetup.setJar("s3://us-east-1.elasticmapreduce/libs/script-runner/script-runner.jar");
        jarSetup.setArgs(sparkArguments);
        sparkConfig.setHadoopJarStep(jarSetup);
        return sparkConfig;
    }

    public boolean executeJob() {
        String stepName = this.getClass().getSimpleName() + "-" + this.marketplaceId;
        StepConfig config = this.createSparkStepConfig(stepName,
                this.getJar(),
                this.getClazz(),
                this.getArgs()
        );
        String stepId = EMRManager.getInstance().addSingleStep(Constants.CLUSTER_ID, config);
        LOGGER.info("Job StepId: " + stepId + " and cluster Id : " + Constants.CLUSTER_ID);
        int jobStatus = this.checkStatus(stepName, stepId);
        if (0 != jobStatus) {
            LOGGER.error("Step failed");
            return false;
        }
        return true;
    }

    protected abstract String getJar();

    protected abstract String getClazz();

    protected abstract String[] getArgs();
}
