package com.tm.map.sourcegenerator.launcher;

import com.tm.map.sourcegenerator.processor.job.PurchaseFollowupComputer;

/**
 * @author <a href="mailto:puneet@targetingmantra.com">Puneet Chaurasia</a>
 */
public class PurchaseFollowupSourceComputerLauncher extends AbstractJobLauncher {

    public PurchaseFollowupSourceComputerLauncher(final int marketplaceId, final String startTime) {
        super(marketplaceId, startTime);
    }

    @Override
    protected String getJar() {
        return "s3://tm-emr/jars/source-generator/map-source-generator-processor.jar";
    }

    @Override
    protected String getClazz() {
        return PurchaseFollowupComputer.class.getCanonicalName();
    }

    @Override
    protected String[] getArgs() {
        return new String[] {startTime};
    }
}
