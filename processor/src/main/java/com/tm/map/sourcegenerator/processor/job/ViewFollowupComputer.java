package com.tm.map.sourcegenerator.processor.job;

import com.tm.common.enums.EventType;
import com.tm.emailservice.common.model.MAPNotificationType;
import com.tm.map.sourcegenerator.processor.utility.CommonsUtility;
import com.tm.map.sourcegenerator.common.Constants;
import com.tm.map.sourcegenerator.processor.pojos.EventInfo;
import com.tm.map.sourcegenerator.processor.utility.SourceGeneratorSupportUtility;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaPairRDD;

import java.io.IOException;
import java.util.Calendar;

/**
 * Generates View followup source data for sending MAP notifications to all users for a given mid and specific hour.
 *
 * @author <a href="mailto:puneet@targetingmantra.com">Puneet Chaurasia</a>
 */
public class ViewFollowupComputer {

    private static Logger LOGGER = Logger.getLogger(ViewFollowupComputer.class);
    private String mid;

    public ViewFollowupComputer(final String mid) {
        this.mid = mid;
    }

    public static void main(String[] args) throws IOException {

        String mid = args[0];
        final long startTime = Long.parseLong(args[1]);
        final ViewFollowupComputer viewFollowupComputer = new ViewFollowupComputer(mid);
        viewFollowupComputer.compute(startTime);
    }

    public void compute(final long startTime) throws IOException{
        long calculationStartTime =  startTime - Constants.THREE_HOUR_TIME_IN_MILLIS;

        Calendar calendar = CommonsUtility.getClearedCalender(calculationStartTime);
        LOGGER.info("INFO - Calculating View followups for for time : " + calendar.getTime().toString());

        JavaPairRDD<String, EventInfo> viewFollowupInfoData = SourceGeneratorSupportUtility.calculateSourceData(mid,
                calendar.getTimeInMillis(), EventType.CLICK, this.getClass().getCanonicalName());
        SourceGeneratorSupportUtility.uploadNotificationSourceDataToS3(mid, viewFollowupInfoData, MAPNotificationType.VIEW_FOLLOWUP,
                calendar.getTimeInMillis());
    }

}
