package com.tm.map.sourcegenerator.processor.job;

import com.tm.common.enums.EventType;
import com.tm.emailservice.common.model.MAPNotificationType;
import com.tm.map.sourcegenerator.processor.utility.CommonsUtility;
import com.tm.map.sourcegenerator.common.Constants;
import com.tm.map.sourcegenerator.processor.pojos.EventInfo;
import com.tm.map.sourcegenerator.processor.utility.SourceGeneratorSupportUtility;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.IOException;
import java.util.Calendar;

/**
 * Generates all the source data for sending MAP notifications to all users for a given mid and specific hour.
 *
 * @author <a href="mailto:puneet@targetingmantra.com">Puneet Chaurasia</a>
 */
public class AllMAPNotificationSourceComputer {

    private static Logger LOGGER = Logger.getLogger(AllMAPNotificationSourceComputer.class);
    private String mid;

    public AllMAPNotificationSourceComputer(final String mid) {
        this.mid = mid;
    }

    public static void main(String[] args) throws IOException {

        String mid = args[0];
        final long startTime = Long.parseLong(args[1]);
        final AllMAPNotificationSourceComputer allMapNotificationSourceComputer = new AllMAPNotificationSourceComputer(mid);
        allMapNotificationSourceComputer.compute(startTime);
    }

    public void compute(final long startTime) throws IOException{
        long calculationStartTime =  startTime - Constants.THREE_HOUR_TIME_IN_MILLIS;

        Calendar calendar = CommonsUtility.getClearedCalender(calculationStartTime);
        LOGGER.info("INFO - Calculating All MAP NotificationSources for time : " + calendar.getTime().toString());

        generateMAPNotificationSourceData(mid, calendar.getTimeInMillis());
    }


    /**
     * Generates and uploads the source data for all the MAP Notifications.
     * Currently supports abandonedcart, viewfollowup and purchasefollowup.
     *
     * @param mid MarketplaceId in context.
     * @param time time for which the source data has to be generated
     * @throws IOException
     */
    public void generateMAPNotificationSourceData(final String mid, final long time) throws IOException {

        JavaSparkContext sparkContext = CommonsUtility.createContext(String.format("%s-%s", mid, this.getClass().getCanonicalName()));

        JavaPairRDD<String, EventInfo> purchasesInfoMap = EventInfo.getEventInfoData(sparkContext, mid, time, EventType.PURCHASE);
        calculateAndUploadPurchaseFollowUpSources(purchasesInfoMap, time);

        JavaPairRDD<String, EventInfo> nextHourPurchasesInfoMap = EventInfo.getEventInfoData(sparkContext, mid,
                time + Constants.ONE_HOUR_TIME_IN_MILLIS, EventType.PURCHASE);
        JavaPairRDD<String, EventInfo> finalPurchasesInfoMap = SourceGeneratorSupportUtility.mergePurchaseData(purchasesInfoMap, nextHourPurchasesInfoMap);

        calculateAndUploadAbandonedCartSources(sparkContext, finalPurchasesInfoMap, time);
        calculateAndUploadViewFollowupSources(sparkContext, finalPurchasesInfoMap, time);

    }

    /**
     * Calculates the AbandonedCart source data and then uploads it to S3.
     *
     * @param purchasesInfoMap All user's purchase information to be taken into account for PurchaseFollowup data calculation.
     * @param time time for which the source data has to be generated
     */
    private void calculateAndUploadPurchaseFollowUpSources(final JavaPairRDD<String, EventInfo> purchasesInfoMap, final long time) {
        SourceGeneratorSupportUtility.uploadNotificationSourceDataToS3(mid, purchasesInfoMap, MAPNotificationType.PURCHASE_FOLLOWUP, time);
    }

    /**
     * Calculates the AbandonedCart source data and then uploads it to S3.
     *
     * @param sparkContext Java Spark Context to be used.
     * @param finalPurchasesInfoMap All user's purchase information to be taken into account for abandoned cart data calculation.
     * @param time time for which the source data has to be generated
     */
    private void calculateAndUploadAbandonedCartSources(final JavaSparkContext sparkContext, final JavaPairRDD<String, EventInfo> finalPurchasesInfoMap, final long time) {
        JavaPairRDD<String, EventInfo> addToCartInfoMap = EventInfo.getEventInfoData(sparkContext, mid, time, EventType.ADDTOCART);
        JavaPairRDD<String, EventInfo> abandonedCartData = SourceGeneratorSupportUtility.calculateDiffFromPurchaseData(finalPurchasesInfoMap,
                addToCartInfoMap, EventType.ADDTOCART);
        SourceGeneratorSupportUtility.uploadNotificationSourceDataToS3(mid, abandonedCartData, MAPNotificationType.ABANDONED_CART, time);
    }

    /**
     * Calculates the ViewFollowup source data and then uploads it to S3.
     *
     * @param sparkContext Java Spark Context to be used.
     * @param finalPurchasesInfoMap All user's purchase information to be taken into account for ViewFollowup data calculation.
     * @param time time for which the source data has to be generated
     */
    private void calculateAndUploadViewFollowupSources(final JavaSparkContext sparkContext, final JavaPairRDD<String, EventInfo> finalPurchasesInfoMap, final long time) {
        JavaPairRDD<String, EventInfo> viewedProductsInfoMap = EventInfo.getEventInfoData(sparkContext, mid, time, EventType.CLICK);
        JavaPairRDD<String, EventInfo> viewFollowUpData = SourceGeneratorSupportUtility.calculateDiffFromPurchaseData(finalPurchasesInfoMap,
                viewedProductsInfoMap, EventType.CLICK);
        SourceGeneratorSupportUtility.uploadNotificationSourceDataToS3(mid, viewFollowUpData, MAPNotificationType.VIEW_FOLLOWUP, time);
    }

}
