package com.tm.map.sourcegenerator.processor.utility;

import com.google.common.base.Optional;
import com.tm.common.enums.EventType;
import com.tm.emailservice.common.model.MAPNotificationType;
import com.tm.map.sourcegenerator.common.Constants;
import com.tm.map.sourcegenerator.common.S3Utility;
import com.tm.map.sourcegenerator.processor.pojos.EventInfo;
import org.apache.hadoop.mapred.FileAlreadyExistsException;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Support utility containing common methods used by to generate Map Notification sources.
 *
 * @author <a href="mailto:puneet@targetingmantra.com">Puneet Chaurasia</a>
 */
public class SourceGeneratorSupportUtility {

    private static Logger LOGGER = Logger.getLogger(SourceGeneratorSupportUtility.class);

    /**
     * Removes all the purchased products from accessed products data.
     *
     * @param purchaseData purchased products data that needs to be removed from productAccessEvents.
     * @param productAccessEvents accessed products data that needs filtered.
     * @param finalDiffEventType event type of products accessed.
     *
     * @return RDD containing purchased products filtered out from accessed products.
     */
    public static JavaPairRDD<String, EventInfo> calculateDiffFromPurchaseData(final JavaPairRDD<String, EventInfo> purchaseData,
                                                                               final JavaPairRDD<String, EventInfo> productAccessEvents,
                                                                               final EventType finalDiffEventType) {

        LOGGER.info("INFO - Entered calculateDiffFromPurchaseData method.");
        JavaPairRDD<String, EventInfo> totalProductAccessEventsNotPurchased;
        if(purchaseData == null){
            //Add all products added to cart in abandoned products list
            LOGGER.info("INFO - Purchase data found to be null. returning all products accessed to Diff list");
            totalProductAccessEventsNotPurchased = productAccessEvents;
        } else {

            JavaPairRDD<String, Tuple2<EventInfo, Optional<EventInfo>>> possibleAccessEventsWithPurchaseMapping;
            try {
                possibleAccessEventsWithPurchaseMapping = productAccessEvents.leftOuterJoin(purchaseData);
            } catch (NullPointerException e) {
                LOGGER.warn("Cannot calculate diff in the data.", e);
                return null;
            }

            totalProductAccessEventsNotPurchased = removeOptionalEventsFromTotalEvents( possibleAccessEventsWithPurchaseMapping, finalDiffEventType );
        }
        LOGGER.info("INFO - Exiting calculateDiffFromPurchaseData method.");
        return totalProductAccessEventsNotPurchased;
    }

    /**
     * Removes data contained in Optional filed from the EventInfo data.
     * i.e if the optional data contains all the products that are purchsed by a user and enventInfo data contains
     * all the products that are viewed/addedToCart, then these purchased prducts will be removed from all the viewed/addedToCart
     * for each user.
     *
     * @param possibleAccessEventsWithPurchaseMapping RDD contaning both type of events's mapping against a user.
     * @param finalDiffEventType event type of the data that are segregated from the purchase data.
     * @return RDD that contains purchased products filtered out.
     */
    public static JavaPairRDD<String, EventInfo> removeOptionalEventsFromTotalEvents( final JavaPairRDD<String, Tuple2<EventInfo,
            Optional<EventInfo>>> possibleAccessEventsWithPurchaseMapping,  final EventType finalDiffEventType) {
        return possibleAccessEventsWithPurchaseMapping.flatMapToPair(
            possibleNotPurchasedEventDetails -> {
                List<Tuple2<String, EventInfo>> notPurchasedEventInfoList = new ArrayList<>(1);
                String userId = possibleNotPurchasedEventDetails._1();
                EventInfo accessedEventInfo = possibleNotPurchasedEventDetails._2()._1();

                EventInfo notPurchasedEventInfo = new EventInfo(finalDiffEventType);
                notPurchasedEventInfo.setUserId(userId);
                notPurchasedEventInfo.setProductIds(new HashSet<>());

                if (possibleNotPurchasedEventDetails._2()._2().isPresent()) {
                    EventInfo purchaseInfo = possibleNotPurchasedEventDetails._2()._2().get();
                    accessedEventInfo.getProductIds().forEach(
                            (productId) -> {
                                if (!purchaseInfo.getProductIds().remove(productId)) {
                                    notPurchasedEventInfo.getProductIds().add(productId);
                                }
                            }
                    );

                } else {
                    notPurchasedEventInfo.setProductIds(accessedEventInfo.getProductIds());
                    notPurchasedEventInfoList.add(new Tuple2<>(userId, notPurchasedEventInfo));
                }
                return notPurchasedEventInfoList;
            });
    }


    /**
     * Merges the purchase data of the two RDD's into one containing distinct users with merged product details.
     *
     * @param purchaseData RDD containing purchase data.
     * @param purchaseDataNextHour RDD contaning purchased data for the next hour
     * @return
     */
    public static JavaPairRDD<String, EventInfo> mergePurchaseData(final JavaPairRDD<String, EventInfo> purchaseData,
                                                                   final JavaPairRDD<String, EventInfo> purchaseDataNextHour) {

        LOGGER.info("INFO - Entered mergePurchaseData method.");
        if(null == purchaseData){
            return purchaseDataNextHour;
        } else if(null == purchaseDataNextHour) {
            return purchaseData;
        }

        JavaPairRDD<String, EventInfo> mergedPurchaseData = getDistinctUserData( purchaseData.union(purchaseDataNextHour) );
        LOGGER.info("INFO - Exiting mergePurchaseData method.");
        return mergedPurchaseData;
    }

    /**
     * Merges all the products for a user to a single value.
     *
     * @param rddContainingDuplicateUsers RDD that may contain duplicate userId's with different products accessed.
     * @return RDD containing distinct values of the user by merging all the set of products for that user.
     */
    public static JavaPairRDD<String, EventInfo> getDistinctUserData( final JavaPairRDD<String, EventInfo> rddContainingDuplicateUsers) {
        return rddContainingDuplicateUsers.reduceByKey(
                (purchasesInfo, otherPurchaseInfo) -> {
                    /*If this function is entered at all, it means that purchasesInfo and
                     nextHourPurchaseInfo both will contain valid values.*/
                    purchasesInfo.getProductIds().addAll(otherPurchaseInfo.getProductIds());
                    return purchasesInfo;
                }
        );
    }

    /**
     * Calculates notification source data for the Event type and time provided.
     *
     * @param mid Marketplace Id in context.
     * @param time time (specific upto hours) for which the sources are to be calculated.
     * @param eventType Event type used for calculating sources.
     * @return RDD containing notification data sources against each userId.
     */
    public static JavaPairRDD<String, EventInfo> calculateSourceData(String mid, long time, final EventType eventType, final String className) {

        JavaSparkContext sparkContext = CommonsUtility.createContext(String.format("%s-%s", mid, className));
        LOGGER.info("INFO - Starting to calculate view followup details.");
        JavaPairRDD<String, EventInfo> accessedProductsInfoMap = EventInfo.getEventInfoData(sparkContext, mid, time, eventType);
        if(accessedProductsInfoMap == null) {
            LOGGER.info("INFO - No Accessed products info found for event : " + eventType.getEventName());
            return null;
        } else if (eventType == EventType.PURCHASE) {
            return accessedProductsInfoMap;
        }
        JavaPairRDD<String, EventInfo> purchasesInfoMap = EventInfo.getEventInfoData(sparkContext, mid, time, EventType.PURCHASE);
        JavaPairRDD<String, EventInfo> nextHourPurchasesInfoMap = EventInfo.getEventInfoData(sparkContext, mid,
                time + Constants.ONE_HOUR_TIME_IN_MILLIS, EventType.PURCHASE);

        JavaPairRDD<String, EventInfo> finalPurchasesInfoMap = SourceGeneratorSupportUtility.mergePurchaseData(purchasesInfoMap, nextHourPurchasesInfoMap);
        return SourceGeneratorSupportUtility.calculateDiffFromPurchaseData(finalPurchasesInfoMap, accessedProductsInfoMap, eventType);
    }

    /**
     * Uploads the generated source data to it respective S3 location
     *
     * @param mid Marketplace id.
     * @param userAbandonedCartInfo RDD containing source data to be uploaded.
     * @param notificationType Type of source data.
     * @param calendar Source data calculation time.
     */
    public static void uploadNotificationSourceDataToS3(final String mid, final JavaPairRDD<String, EventInfo> userAbandonedCartInfo,
                                                        final MAPNotificationType notificationType, final long time) {

        if(userAbandonedCartInfo == null) {
            LOGGER.warn("INFO - FINAL " + notificationType.getMapNotificationName() + " size is found NULL. Nothing to upload. Returning.");
            return;
        }

        String s3Key = Constants.EMAIL_SOURCE_S3_PATH.replace(Constants.MID_MACRO, mid);
        String dirDatePath = Constants.DATE_TIME_DIR_FORMATTER.print(time);
        s3Key = s3Key.replace(Constants.DATE_MACRO, dirDatePath);
        s3Key = s3Key.replace(Constants.EMAIL_MACRO, notificationType.getMapNotificationName());
        String s3Path = Constants.S3_PREFIX + Constants.EMAIL_SOURCE_S3_BUCKET + Constants.URL_SEPARATOR + s3Key;
        LOGGER.info("S3 Path to store final " + notificationType.getMapNotificationName() + " sources is :" + s3Path);
        try {
            JavaPairRDD<String,EventInfo> sourceDataWithSinglePartitionedValues =
                    SourceGeneratorSupportUtility.getDistinctUserData(userAbandonedCartInfo.coalesce(1));
            sourceDataWithSinglePartitionedValues.values().saveAsTextFile(s3Path);
        } catch (Exception e) {
            if(e instanceof FileAlreadyExistsException) {
                LOGGER.info("File already exists. Attempting to delete before retrying.");
                S3Utility.getInstance().deletePathIfExists(s3Path);
                uploadNotificationSourceDataToS3(mid, userAbandonedCartInfo, notificationType, time);
                return;
            } else {
                throw e;
            }
        }
        LOGGER.info("INFO - Save to S3 successful for source : " + notificationType.getMapNotificationName());
    }
}
