package com.tm.map.sourcegenerator.processor.job;

import com.tm.common.enums.EventType;
import com.tm.emailservice.common.model.MAPNotificationType;
import com.tm.map.sourcegenerator.processor.utility.CommonsUtility;
import com.tm.map.sourcegenerator.common.Constants;
import com.tm.map.sourcegenerator.processor.pojos.EventInfo;
import com.tm.map.sourcegenerator.processor.utility.SourceGeneratorSupportUtility;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaPairRDD;

import java.io.IOException;
import java.util.Calendar;

/**
 * Generates Purchase followup source data for sending MAP notifications to all users for a given mid and specific hour.
 *
 * @author <a href="mailto:puneet@targetingmantra.com">Puneet Chaurasia</a>
 */
public class PurchaseFollowupComputer {

    private static Logger LOGGER = Logger.getLogger(PurchaseFollowupComputer.class);
    private String mid;

    public PurchaseFollowupComputer(final String mid) {
        this.mid = mid;
    }

    public static void main(String[] args) throws IOException {

        String mid = args[0];
        final long startTime = Long.parseLong(args[1]);
        final PurchaseFollowupComputer purchaseFollowupComputer = new PurchaseFollowupComputer(mid);
        purchaseFollowupComputer.compute(startTime);
    }

    public void compute(final long startTime) throws IOException{
        long calculationStartTime =  startTime - Constants.THREE_HOUR_TIME_IN_MILLIS;

        Calendar calendar = CommonsUtility.getClearedCalender(calculationStartTime);
        LOGGER.info("INFO - Calculating Purchase followups for time : " + calendar.getTime().toString());

        JavaPairRDD<String, EventInfo> purchaseFollowupInfoData = SourceGeneratorSupportUtility.calculateSourceData(mid,
                calendar.getTimeInMillis(), EventType.PURCHASE, this.getClass().getCanonicalName());
        SourceGeneratorSupportUtility.uploadNotificationSourceDataToS3(mid, purchaseFollowupInfoData, MAPNotificationType.PURCHASE_FOLLOWUP,
                calendar.getTimeInMillis());
    }

}
