package com.tm.map.sourcegenerator.processor.utility;

import com.tm.map.sourcegenerator.common.Constants;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.Calendar;

/**
 * Contains common utility methods to be used across this module.
 */
public class CommonsUtility {

    /**
     * Creates and returns a calender object set to the given timestamp after clearing out its previous values.
     *
     * @param timeToSetInMillis timeStamp in millis to be used for calender setting.
     * @return A calender object set to the given timestamp
     */
    public static Calendar getClearedCalender(final long timeToSetInMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.setTimeInMillis(timeToSetInMillis);
        return calendar;
    }

    /**
     * Creates the Java Spark Context to be used for the given appName.
     *
     * @param appName to be be used as the EMR's step name.
     * @return spark context using hadoop configurations.
     */
    public static JavaSparkContext createContext(String appName) {
        SparkConf conf = new SparkConf().setAppName(appName);
        JavaSparkContext context = new JavaSparkContext(conf);
        context.hadoopConfiguration().set("fs.s3.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem");
        context.hadoopConfiguration().set("fs.s3.awsAccessKeyId", Constants.AWS_ACCESS_KEY);
        context.hadoopConfiguration().set("fs.s3.awsSecretAccessKey", Constants.AWS_SECRET_KEY);
        return context;
    }

}
