package com.tm.map.sourcegenerator.processor.pojos;

import com.google.common.base.Joiner;
import com.tm.common.enums.EventType;
import com.tm.map.sourcegenerator.common.Constants;
import com.tm.map.sourcegenerator.common.PathUtils;
import com.tm.map.sourcegenerator.common.S3Utility;
import lombok.Data;
import lombok.NonNull;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Contains event members that are finally saved as the source data for MAP notification.
 */
@Data
public class EventInfo implements Serializable {

    private static Logger LOGGER = Logger.getLogger(EventInfo.class);

    private EventType eventType;
    private String userId;
    private Set<String> productIds;

    public EventInfo(@NonNull EventType eventType) {
        this.eventType = eventType;
    }

    @Override
    public String toString() {
        String productIds = "";
        productIds = productIds + String.join(Constants.PIPE_SEP, getProductIds());
        return getUserId() + Constants.KEY_SEP + productIds;
    }

    /**
     * Downloads the given event's behavior data from S3 and constructs the EventInfo RDD out of the valid records present in that data.
     *
     * @param sparkContext Java Spark Context to access behavior data.
     * @param mid Marketplace ID in context.
     * @param time time (specific to an hour) for which the behavior data has to be accessed.
     * @param eventType Event type for which the behavior data has to be accessed.
     *
     * @return RDD containing userId to his EventInfo mapping for the given eventType and time.
     */
    public static JavaPairRDD<String, EventInfo> getEventInfoData(@NonNull final JavaSparkContext sparkContext, @NonNull final String mid, @NonNull final long time, @NonNull final EventType eventType) {

        LOGGER.info("INFO - Entered getEventInfoData method for event and time as :" + eventType.getEventName() + " " + time);
        String key = PathUtils.constructS3Key(Constants.BEHAVIOR_DATA_S3_PATH, mid, time, eventType.getEventId());
        List<String> s3Paths = S3Utility.getInstance().getPaths(Constants.BEHAVIOR_DATA_S3_BUCKET, key);
        if(s3Paths != null && !s3Paths.isEmpty()) {
            LOGGER.info("INFO - S3 paths are : ");
            for(int i = 0 ; i < s3Paths.size(); ++i){
                LOGGER.info(s3Paths.get(i));
            }
        } else {
            LOGGER.info("Found empty list of S3 paths.");
            return null;
        }
        JavaPairRDD<String, EventInfo> userToAddToCartRDD = sparkContext.textFile(Joiner.on(',').join(s3Paths))
                .flatMapToPair( line -> {
                    List<Tuple2<String, EventInfo>> list = null;
                    String[] record = line.split(",",-1);
                    if(containsValidEventInfoEntries(record)) {
                        EventInfo eventInfo = new EventInfo(eventType);
                        eventInfo.setUserId(record[0]);
                        Set<String> productIds = new HashSet<>();
                        productIds.add(record[2]);
                        eventInfo.setProductIds(productIds);

                        Tuple2<String, EventInfo> currentTuple = new Tuple2<>(eventInfo.getUserId(), eventInfo);
                        list = new ArrayList<>(1);
                        list.add(currentTuple);
                    } else {
                        LOGGER.debug("Record or user id is null or empty for event :" + eventType.getEventName() + ". Line is : " + line);
                        list = new ArrayList<>(0);
                    }

                    return list;
                });
        LOGGER.info("INFO - Exiting getEventInfoData method for event and time as :" + eventType.getEventName() + " " + time);
        return userToAddToCartRDD;
    }

    /**
     * Validates the EventInfo record.
     *
     * @param record Event Info record array.
     * @return true if the record is valid, false otherwise.
     */
    private static boolean containsValidEventInfoEntries(final String[] record) {
        return record.length >= 2 && null != record[0] && !record[0].isEmpty() && !record[0].equalsIgnoreCase("null");
    }
}
