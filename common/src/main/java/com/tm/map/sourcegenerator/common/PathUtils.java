package com.tm.map.sourcegenerator.common;

import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.tm.common.aws.s3.S3Manager;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class PathUtils {

    private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("yyyy/MM/dd");
    private static final Logger LOGGER = Logger.getLogger(PathUtils.class);

    public static String constructS3Key(String s3BehaviorDataPath, String mid, long time, int event) {
        String dayPart = Constants.DATE_TIME_DIR_FORMATTER.print(time);
        return s3BehaviorDataPath.replace(Constants.MID_MACRO, mid).replace(Constants.DATE_MACRO, dayPart).
                replace(Constants.EVENT_MACRO, Integer.toString(event));
    }

    public static List<String> getBehaviorDataPath(String behaviorFolder, int event) {
        String dirPath = behaviorFolder + File.separator + event + File.separator + FORMATTER.print(getCurrentTime().minusDays(1));
        return getFiles(dirPath);
    }

    public static DateTime getCurrentTime() {
        return DateTime.now(Constants.IST_TIME_ZONE);
    }

    private static List<String> getFiles(String dirPath) {
        List<String> paths = new ArrayList();
        ObjectListing summaries = S3Manager.getInstance(Constants.AWS_ACCESS_KEY, Constants.AWS_SECRET_KEY).getListObjects(dirPath);
        if (summaries != null && summaries.getObjectSummaries().size() > 0) {
            for (S3ObjectSummary summary : summaries.getObjectSummaries()) {
                paths.add(Constants.S3_PREFIX + summary.getBucketName() + File.separator + summary.getKey());
            }
        }
        List<String> s3Paths = new ArrayList(paths);
        for (String path: s3Paths) {
            LOGGER.info("[ATTENTION] Path is: " + path);
            if (path.contains("_SUCCESS")) {
                paths.remove(path);
            }
        }
        if (paths.isEmpty()) {
            LOGGER.info("[ATTENTION] Path is empty for dir : " + dirPath);
        }
        return paths;
    }
}
