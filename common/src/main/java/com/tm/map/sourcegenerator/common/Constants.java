package com.tm.map.sourcegenerator.common;

import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.TimeZone;

/**
 * @author <a href="mailto:puneet@targetingmantra.com">Puneet Chaurasia</a>
 */
public class Constants {

    public static final String MID_MACRO = "$MID";
    public static final String DATE_MACRO = "$DATE";
    public static final String EVENT_MACRO = "$EVENT";
    public static final String EMAIL_MACRO = "$EMAIL_TYPE";

    public static final DateTimeZone IST_TIME_ZONE = DateTimeZone.forTimeZone(TimeZone.getTimeZone("GMT+5:30"));
    public static final DateTimeFormatter DATE_TIME_DIR_FORMATTER = DateTimeFormat.forPattern("yyyy/MM/dd/HH").withZone(IST_TIME_ZONE);


    public static final String AWS_ACCESS_KEY = "AKIAJ5L2T7SC74DHSGQA";
    public static final String AWS_SECRET_KEY = "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";
    public static final String CLUSTER_ID = "j-3IS92RRJQK5EZ"; //Email Prediction Cluster

    public static final long ONE_HOUR_TIME_IN_MILLIS = 3600000l;
    public static final long THREE_HOUR_TIME_IN_MILLIS = ONE_HOUR_TIME_IN_MILLIS * 3;
    public static final long FOUR_HOUR_TIME_IN_MILLIS = ONE_HOUR_TIME_IN_MILLIS * 4;

    public static final String S3_PREFIX = "s3://";

    public static final String BEHAVIOR_DATA_S3_BUCKET = "tmcd";
    public static final String EMAIL_SOURCE_S3_BUCKET = "puneettest";
    //DATE is in 2014/10/10 format
    //EMAIL_TYPE abandonedcart, purchasefollowup, viewfollowup
    public static final String BEHAVIOR_DATA_S3_PATH = "$MID/behaviorData/$EVENT/$DATE";
    public static final String EMAIL_SOURCE_S3_PATH = "$MID/emailSource/$EMAIL_TYPE/$DATE";

    public static final String KEY_SEP = ":";
    public static final String PIPE_SEP = "|";
    public static final String URL_SEPARATOR = "/";
}
