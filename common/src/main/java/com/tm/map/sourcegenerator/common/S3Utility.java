package com.tm.map.sourcegenerator.common;

import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.tm.common.aws.s3.S3Manager;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by puneet on 1/5/16.
 */
public class S3Utility extends S3Manager {


    private static final Logger LOGGER = Logger.getLogger(S3Manager.class);
    private static S3Utility instance;

    protected S3Utility(String accessKey, String secretKey) {
        super(accessKey, secretKey);
    }

    public static S3Utility getInstance() {
        if (instance == null) {
            synchronized (S3Manager.class) {
                if (instance == null) {
                    instance = new S3Utility(Constants.AWS_ACCESS_KEY, Constants.AWS_SECRET_KEY);
                }
            }
        }
        return instance;
    }

    public List<String> getAllKeys(final String bucket, final String path){
        LOGGER.info("S3 Bucket is " + bucket +  " and path is " + path);
        ObjectListing objectListing = s3Client
                .listObjects(new ListObjectsRequest().withBucketName(bucket).withPrefix(path));
        List<String> s3Keys = new ArrayList<>();
        for (S3ObjectSummary objectSummary : objectListing
                .getObjectSummaries()) {
            s3Keys.add(objectSummary.getKey());
        }
        return s3Keys;
    }

    public List<String> getPaths(final String bucket, final String path) {
        List<String> keys = getAllKeys(bucket, path);
        if(keys == null || keys.isEmpty()){
            return keys;
        } else {
            List<String> paths = new ArrayList<>(keys.size());
            for(String key : keys){
                paths.add(Constants.S3_PREFIX + bucket + Constants.URL_SEPARATOR + key);
            }
            return paths;
        }
    }
}
